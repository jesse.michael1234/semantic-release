## Semantic Release 📦 🚀

---

🐳 Dockerize the [Semantic-Release](https://github.com/semantic-release/semantic-release) Node tool.

⚙️ [Configured](https://semantic-release.gitbook.io/semantic-release/usage/configuration) in the [.releaserc.json](.releaserc.json) file.

🏗 Built with [plugins](https://semantic-release.gitbook.io/semantic-release/extending/plugins-list).
- [Git](https://github.com/semantic-release/git)
- [Gitlab](https://github.com/semantic-release/gitlab)
- [Changelog](https://github.com/semantic-release/changelog)
