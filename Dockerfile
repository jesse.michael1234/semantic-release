FROM node:13-alpine

RUN apk add --no-cache git openssh

COPY .releaserc.json .

RUN npm install -g semantic-release @semantic-release/gitlab @semantic-release/changelog @semantic-release/git 

RUN npm cache clean --force
